class Solution:
    def minimumDeletions(self, nums: list[int]) -> int:
        size = len(nums)
        if size < 3:
            return size

        min_value = nums[0]
        min_position = 0
        max_value = nums[0]
        max_position = 0

        for i in range(size):
            if nums[i] < min_value:
                min_value = nums[i]
                min_position = i
                continue

            if nums[i] > max_value:
                max_value = nums[i]
                max_position = i

        half_size = size / 2

        right_pos = max(min_position, max_position)
        if right_pos <= half_size:
            return right_pos + 1

        left_pos = min(min_position, max_position)
        if left_pos >= half_size:
            return size - left_pos

        return min(left_pos + (size - right_pos) + 1, size - left_pos, right_pos + 1)

<?php

class Solution
{

    public function search($nums, $target)
    {
        $pivot = $nums[0];

        if ($pivot === $target) {
            return 0;
        }

        $length = count($nums);

        if ($length === 1) {
            return -1;
        }

        $start = 1;
        $end = $length - 1;
        while (true) {
            $mdlIndex = $start + (int) (($end - $start) / 2);

            $mdl = $nums[$mdlIndex];

            if ($mdl === $target) {
                return $mdlIndex;
            }

            if ($start >= $end) {
                break;
            }

            if ($target > $pivot && $pivot > $mdl) {
                $end = $mdlIndex - 1;
                continue;
            }

            if ($target < $pivot && $pivot < $mdl) {
                $start = $mdlIndex + 1;
                continue;
            }

            if ($mdl > $target) {
                $end = $mdlIndex - 1;
            } else {
                $start = $mdlIndex + 1;
            }
        }

        return -1;
    }
}
<?php

class Solution
{

    public function minPathSum($grid)
    {
        $yAxisLength = count($grid);
        $xAxisLength = count($grid[0]);

        $matrix = [];
        for ($y = 0; $y < $yAxisLength; $y++) {
            for ($x = 0; $x < $xAxisLength; $x++) {
                if ($y == 0 && $x == 0) {
                    $matrix[$y][$x] = $grid[0][0];
                    continue;
                }

                $matrix[$y][$x] = min(INF, $matrix[$y][$x - 1] ?? INF, $matrix[$y - 1][$x] ?? INF) + $grid[$y][$x];
            }
        }

        return $matrix[$yAxisLength - 1][$xAxisLength - 1];
    }

}
<?php

class Solution {

    /**
     * @param String $s
     * @return String
     */
    function longestPalindrome($s)
    {
        $lengthOfString = strlen($s);

        if ($lengthOfString === 1) {
            return $s;
        }

        if ($lengthOfString === 2 && $s[0] === $s[1]) {
            return $s;
        }

        $start = 0;
        $max = 1;
        for ($i = 0; $i < $lengthOfString - 1; $i++) {
            if ($max === $lengthOfString) {
                break;
            }

            $iteration = 0;

            $tmpMaxOdd = 1;
            $tmpMaxEven = 0;

            $isEqualOdd = true;
            $isEqualEven = true;

            while (($prevIteration = $i - $iteration) >= 0 && ($nextIteration = $i + $iteration) + 1 < $lengthOfString) {
                if ($isEqualOdd) {
                    if ($prevIteration - 1 >= 0 && $s[$prevIteration - 1] === $s[$nextIteration + 1]) {
                        $tmpMaxOdd += 2;
                    } else {
                        $isEqualOdd = false;
                    }
                }

                if ($isEqualEven) {
                    if ($s[$prevIteration] === $s[$nextIteration + 1]) {
                        $tmpMaxEven += 2;
                    } else {
                        $isEqualEven = false;
                    }
                }

                if ($tmpMaxOdd > $max) {
                    $start = $prevIteration - 1;
                    $max = $tmpMaxOdd;
                }

                if ($tmpMaxEven > $max) {
                    $start = $prevIteration;
                    $max = $tmpMaxEven;
                }

                if (!$isEqualOdd && !$isEqualEven) {
                    break;
                }

                $iteration++;
            }
        }

        return substr($s, $start, $max);
    }
}
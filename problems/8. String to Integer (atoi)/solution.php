<?php

class Solution {

    /**
     * @param String $s
     * @return Integer
     */
    function myAtoi($s)
    {
        $numberAsString = (string) $s;
        $numberLength = strlen($numberAsString);

        $isNegativeNumber = null;
        $intAsString = '';
        for ($i = 0; $i < $numberLength; $i++) {
            if (is_numeric($numberAsString[$i])) {
                $intAsString .= $numberAsString[$i];
                continue;
            }

            if (strlen($intAsString) || $isNegativeNumber !== null) {
                break;
            }

            if ($numberAsString[$i] === ' ') {
                continue;
            }

            if ($numberAsString[$i] === '-' || $numberAsString[$i] === '+') {
                $isNegativeNumber = $numberAsString[$i] === '-';
                continue;
            }

            break;
        }

        $intAsString = ltrim($intAsString, '0');

        $maxModuleInt = !$isNegativeNumber ? 2147483647 : 2147483648;
        $maxIntAsString = (string) $maxModuleInt;
        $intLength = strlen($intAsString);

        if ($intLength < 10) {
            return !$isNegativeNumber ? (int) $intAsString : ((int) $intAsString) * -1;
        }

        if ($intLength > 10) {
            return !$isNegativeNumber ? $maxModuleInt : $maxModuleInt * -1;
        }

        for ($i = 0; $i < $intLength; $i++) {
            if ($intAsString[$i] < $maxIntAsString[$i]) {
                break;
            }

            if ($intAsString[$i] > $maxIntAsString[$i]) {
                return !$isNegativeNumber ? $maxModuleInt : $maxModuleInt * -1;
            }
        }

        return !$isNegativeNumber ? (int) $intAsString : ((int) $intAsString) * -1;
    }
}
<?php

class Solution
{

    public function subsets($nums)
    {
        $result = [];
        foreach ($nums as $num) {
            foreach ($result as $item) {
                $item[] = $num;
                $result[] = $item;
            }

            $result[] = [$num];
        }

        $result[] = [];

        return $result;
    }

}
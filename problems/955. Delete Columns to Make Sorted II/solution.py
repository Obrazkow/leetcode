class Solution:

    def __init__(self):
        self.__chars_maps = {
            'a': 0,
            'b': 1,
            'c': 2,
            'd': 3,
            'e': 4,
            'f': 5,
            'g': 6,
            'h': 7,
            'i': 8,
            'j': 9,
            'k': 10,
            'l': 11,
            'm': 12,
            'n': 13,
            'o': 14,
            'p': 15,
            'q': 16,
            'r': 17,
            's': 18,
            't': 19,
            'u': 20,
            'v': 21,
            'w': 22,
            'x': 23,
            'y': 24,
            'z': 25
        }

    def minDeletionSize(self, words: list[str]) -> int:
        if not len(words):
            return 0

        deleted_positions = set()

        self.__delete_unsorted_positions(deleted_positions, 0, words, 0, len(words))

        return len(deleted_positions)

    def __delete_unsorted_positions(
        self,
        deleted_positions: set,
        position: int,
        words: list[str],
        start: int,
        end: int
    ) -> None:
        if len(words[0]) == position:
            return

        if position in deleted_positions:
            self.__delete_unsorted_positions(deleted_positions, position + 1, words, start, end)
            return

        prev_value = -1

        position_of_same_chars_sequence = None
        for word_number in range(start, end):
            word = words[word_number]
            value = self.__chars_maps[word[position]]
            if value < prev_value:
                deleted_positions.add(position)
                self.__delete_unsorted_positions(
                    deleted_positions,
                    0,
                    words,
                    0,
                    len(words)
                )
                return

            if value == prev_value:
                if position_of_same_chars_sequence is None:
                    position_of_same_chars_sequence = word_number - 1

                continue

            if position_of_same_chars_sequence is not None:
                self.__delete_unsorted_positions(
                    deleted_positions,
                    position + 1,
                    words,
                    position_of_same_chars_sequence,
                    word_number
                )
                position_of_same_chars_sequence = None

            prev_value = value

        if position_of_same_chars_sequence is not None:
            self.__delete_unsorted_positions(
                deleted_positions,
                position + 1,
                words,
                position_of_same_chars_sequence,
                end
            )

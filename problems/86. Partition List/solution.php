<?php

class Solution
{

    public function partition($head, $x)
    {
        $rootRight = new ListNode(0, null);
        $rootLeft = new ListNode(0, null);

        $right = $rootRight;
        $left = $rootLeft;

        while ($head) {
            if ($head->val >= $x) {
                $left->next = $head;
                $left = $head;
            } else {
                $right->next = $head;
                $right = $head;
            }

            $head = $head->next;
        }

        $right->next = null;
        $left->next = null;

        if (!$rootRight->next) {
            return $rootLeft->next;
        }

        if ($rootRight->next && $rootLeft->next) {
            $right->next = $rootLeft->next;
        }

        return $rootRight->next;
    }
}
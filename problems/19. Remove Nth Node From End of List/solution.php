<?php

class Solution {


    public function removeNthFromEnd($head, $n)
    {
        $wrapper = new ListNode(0, $head);

        $i = 0;
        $root = $wrapper;
        $parentOfNodeToDelete = null;
        while(true) {

            if ($parentOfNodeToDelete) {
                $parentOfNodeToDelete = $parentOfNodeToDelete->next;
            } else {
                if ($i++ === $n) {
                    $parentOfNodeToDelete = $root;
                }
            }

            if (!$wrapper->next) {
                break;
            }

            $wrapper = $wrapper->next;
        }

        if ($parentOfNodeToDelete) {
            $parentOfNodeToDelete->next = $parentOfNodeToDelete->next->next;
        }


        return $root->next;
    }
}
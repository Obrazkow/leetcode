<?php

class LRUCache
{

    private $capacity = 0;

    private $size = 0;
    private $data = [];
    private $rear = null;
    private $front = null;

    public function __construct(int $capacity)
    {
        $this->capacity = $capacity;
    }

    public function get(int $key): int
    {
        if (!isset($this->data[$key])) {
            return -1;
        }

        $item = $this->data[$key];
        if ($this->size === 1 || $this->front === $key) {
            return $item[0];
        }

        $value = $item[0];
        $prevIndex = $item[1];
        $nextIndex = $item[2];

        if ($this->rear === $key) {
            $this->data[$nextIndex][1] = null;
            $this->rear = $nextIndex;
        } else {
            $this->data[$prevIndex][2] = $nextIndex;
            $this->data[$nextIndex][1] = $prevIndex;
        }

        $this->data[$this->front][2] = $key;
        $this->data[$key] = [$value, $this->front, null];
        $this->front = $key;

        return $value;
    }

    public function put(int $key, int $value): void
    {
        $storedValue = $this->get($key);
        if ($storedValue !== -1) {
            $this->data[$key][0] = $value;
            return;
        }

        if (!$this->size) {
            $this->rear = $key;
            $this->front = $key;
            $this->data[$key] = [$value, null, null];
            $this->size = 1;
            return;
        }

        $this->data[$key] = [$value, $this->front, null];
        $this->data[$this->front][2] = $key;
        $this->front = $key;

        if (++$this->size > $this->capacity) {
            $rear = $this->rear;
            $this->rear = $this->data[$rear][2];
            unset($this->data[$rear]);
        }
    }

}
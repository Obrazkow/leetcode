<?php

class Solution {

    /**
     * @param String $s
     * @param Integer $numRows
     * @return String
     */
    function convert($s, $numRows)
    {
        $period = $numRows;
        if ($period > 2) {
            $period = 2 + ($period - 2) * 2;
        }

        $result = '';

        for ($i = 0; $i < $numRows; $i++) {
            $iteration = 0;
            $isInnerRow = $i !== 0 && $i !== $numRows - 1;
            while (true) {
                $indexOfCurPeriod = $period * $iteration++;
                $indexOfSymbol = $indexOfCurPeriod + $i;

                if (!isset($s[$indexOfSymbol])) {
                    break;
                }

                $result .= $s[$indexOfSymbol];
                if ($isInnerRow) {
                    $indexOfMirroredSymbol = $indexOfCurPeriod + $period - $i;

                    if (isset($s[$indexOfMirroredSymbol])) {
                        $result .= $s[$indexOfMirroredSymbol];
                    }
                }
            }
        }

        return $result;
    }
}
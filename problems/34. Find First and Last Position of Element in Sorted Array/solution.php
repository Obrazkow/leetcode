<?php

class Solution
{

    public function searchRange($nums, $target)
    {
        $length = count($nums);

        if ($length === 1) {
            return $nums[0] === $target ? [0, 0]: [-1, -1];
        }

        if ($nums[0] > $target) {
            return [-1, -1];
        }

        $start = 0;
        $range = [-1, -1];
        foreach ($range as $key => $value) {
            $end = $length - 1;

            while (true) {
                $mdlIndex = $start + (int) (($end - $start) / 2);
                $mdl = $nums[$mdlIndex];

                if ($mdl === $target) {
                    $range[$key] = $mdlIndex;
                    if ($key === 0) {
                        $end = $mdlIndex - 1;
                        if ($end < $start) {
                            break;
                        }
                    } else {
                        $start = $mdlIndex + 1;
                    }

                    continue;
                }

                if ($start >= $end) {
                    break;
                }

                if ($mdl > $target) {
                    $end = $mdlIndex - 1;
                } else {
                    $start = $mdlIndex + 1;
                }
            }

            if ($range[$key] === -1) {
                return [-1, -1];
            }
        }

        return $range;
    }
}

<?php

class Solution
{

    private $tokens = [];
    private $cursor = 0;

    private const T_BIN_OP = 1;
    private const T_INT = 2;

    private const PRECEDENCE = [
        '+' => 1,
        '-' => 1,
        '*' => 2,
        '/' => 2,
    ];

    public function calculate($expression): int
    {
        $this->tokens = $this->getTokens($expression);

        $node = $this->getNode(0);

        return $this->evaluate($node);
    }

    private function evaluate(array $node): int
    {
        if ($node['root']['t'] === self::T_INT) {
            return $node['root']['v'];
        }

        if ($node['root']['v'] === '+') {
            return $this->evaluate($node['left']) + $this->evaluate($node['right']);
        }

        if ($node['root']['v'] === '-') {
            return $this->evaluate($node['left']) - $this->evaluate($node['right']);
        }

        if ($node['root']['v'] === '*') {
            return (int) $this->evaluate($node['left']) * $this->evaluate($node['right']);
        }

        if ($node['root']['v'] === '/') {
            return (int) $this->evaluate($node['left']) / $this->evaluate($node['right']);
        }

        throw new Exception('Unresolved node');
    }

    private function getNode($precedence): array
    {
        $numberToken = $this->tokens[$this->cursor++];

        $node = [
            'root' => $numberToken
        ];

        while ($this->cursor !== count($this->tokens)) {
            $operatorToken = $this->tokens[$this->cursor];

            if ($precedence < self::PRECEDENCE[$operatorToken['v']]) {
                ++$this->cursor;

                $node = [
                    'root' => $operatorToken,
                    'left' => $node,
                    'right' => $this->getNode(self::PRECEDENCE[$operatorToken['v']]),
                ];
            } else {
                break;
            }
        }

        return $node;
    }


    private function getTokens(string $expression)
    {
        $expression = str_replace(' ', '', $expression);

        $cursor = 0;
        $stringLength = strlen($expression);
        $binaryOps = [
            '+',
            '-',
            '*',
            '/',
        ];

        $binaryOps = array_flip($binaryOps);

        $stack = [];
        while ($cursor < $stringLength) {
            if (isset($binaryOps[$expression[$cursor]])) {
                $stack[] = [
                    't' => self::T_BIN_OP,
                    'v' => $expression[$cursor],
                ];
                $cursor++;
                continue;
            }

            preg_match('/[0-9]*/', $expression, $match, 0, $cursor);
            if (strlen($match[0])) {
                $cursor += strlen($match[0]);
                $stack[] = [
                    't' => self::T_INT,
                    'v' => (int) $match[0]
                ];
                continue;
            }

            throw new Exception(sprintf('Unresolved token %s', $expression[$cursor]));
        }

        return $stack;
    }

}
<?php

class Solution
{

    public function swapPairs($head)
    {
        if (!$head) {
            return $head;
        }

        $items = [];
        while (true) {
            $next = $head->next;
            $head->next = null;
            $items[] = $head;

            if (!$next) {
                break;
            }

            $head = $next;
        }

        $countOfItems = count($items);
        $countOfPairs = (int) ($countOfItems / 2);
        $head = $countOfItems % 2 !== 0 ? $items[$countOfItems - 1] : null;

        for ($i = $countOfPairs - 1; $i >= 0; $i--) {
            $start = $i * 2;

            $child = $items[$start];
            $child->next = $head;
            $tmpHead = $items[$start + 1];
            $tmpHead->next = $child;

            $head = $tmpHead;
        }

        return $head;
    }
}
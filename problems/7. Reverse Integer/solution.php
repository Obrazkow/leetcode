<?php

class Solution {

    /**
     * @param Integer $x
     * @return Integer
     */
    function reverse($x)
    {
        $maxInt = 2147483647;

        $isNegativeNumber = $x < 0;

        if ($isNegativeNumber) {
            $x = $x * -1;

            $maxInt += 1;
        }

        $intAsString = (string) $x;
        $maxIntAsString = (string) $maxInt;
        $intLength = strlen($intAsString);

        $number = '';
        for ($i = 0; $i < $intLength; $i++) {
            $number = $intAsString[$i] . $number;
        }

        if (strlen($number) < 10) {
            return $isNegativeNumber ? (int) $number * -1 : (int) $number;
        }

        for ($i = 0; $i < $intLength; $i++) {
            if ($number[$i] < $maxIntAsString[$i]) {
                break;
            }

            if ($number[$i] > $maxIntAsString[$i]) {
                return 0;
            }
        }

        return $isNegativeNumber ? (int) $number * -1 : (int) $number;
    }
}
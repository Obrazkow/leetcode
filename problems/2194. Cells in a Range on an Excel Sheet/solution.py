class Solution:

    def cellsInRange(self, s: str) -> list[str]:
        start, end = s.split(":")

        start_row = int(start[1])
        end_row = int(end[1])

        result = []
        for i in range(ord(start[0]), ord(end[0]) + 1):
            char = chr(i)
            for j in range(start_row, end_row + 1):
                result.append(char + str(j))

        return result

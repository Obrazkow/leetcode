from __future__ import annotations

from typing import Optional


class Node:
    val: int
    prev: Node | None
    next: Node | None
    child: Node | None


class Solution:
    def flatten(self, head: 'Optional[Node]') -> 'Optional[Node]':
        if head is None:
            return None

        stack = []
        cursor = head
        while True:
            if cursor.child:
                stack.append(cursor)
                cursor = cursor.child
                continue

            if cursor.next:
                tmp = cursor
                cursor = cursor.next
                cursor.prev = tmp
                continue

            if not stack:
                return head

            parent = stack.pop()
            child = parent.child

            cursor.next = parent.next

            parent.next = child
            child.prev = parent
            parent.child = None

<?php

class Solution {

    /**
     * @param Integer[] $nums1
     * @param Integer[] $nums2
     * @return Float
     */
    function findMedianSortedArrays($nums1, $nums2)
    {
        $countOfNumbers = count($nums1) + count($nums2);
        if ($countOfNumbers === 0) {
            return 0;
        }

        $isEven = $countOfNumbers % 2 === 0;
        $medianIndex = (int) (($countOfNumbers / 2) + 1);

        $prev = 0;
        $current = 0;
        $position = 0;
        $leftIndex = 0;
        $rightIndex = 0;
        while ($position++ !== $medianIndex) {
            $prev = $current;
            if (isset($nums1[$leftIndex]) && isset($nums2[$rightIndex])) {
                $current = $nums1[$leftIndex] > $nums2[$rightIndex] ? $nums2[$rightIndex++] : $nums1[$leftIndex++];
                continue;
            }

            if (isset($nums1[$leftIndex])) {
                $current = $nums1[$leftIndex++];
                continue;
            }

            if (isset($nums2[$rightIndex])) {
                $current = $nums2[$rightIndex++];
                continue;
            }
        }

        $result = $isEven ? ($prev + $current) / 2 : $current;

        return (float) $result;
    }
}
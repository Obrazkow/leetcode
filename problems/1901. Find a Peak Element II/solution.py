from typing import List


class Solution:
    def findPeakGrid(self, mat: List[List[int]]) -> List[int]:
        max = mat[0][0]
        point_x = 0
        point_y = 0

        y_length = len(mat)
        x_length = len(mat[0])
        while True:
            found_new = False

            point_x_cursor = 0
            while point_x_cursor < x_length:
                if mat[point_x][point_x_cursor] > max:
                    max = mat[point_x][point_x_cursor]
                    point_y = point_x_cursor
                    found_new = True

                point_x_cursor += 1

            point_y_cursor = 0
            while point_y_cursor < y_length:
                if mat[point_y_cursor][point_y] > max:
                    max = mat[point_y_cursor][point_y]
                    point_x = point_y_cursor
                    found_new = True

                point_y_cursor += 1

            if not found_new:
                return [point_x, point_y]

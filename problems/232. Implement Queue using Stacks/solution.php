<?php

class MyQueue {


    private $firstElement;
    private $lastElement;

    public function push($x)
    {
        if ($this->lastElement === null) {
            $this->lastElement = [
                'v' => $x,
                'n' => null
            ];
        } else {
            $newLast = [
                'v' => $x,
                'n' => null
            ];
            $last = &$this->lastElement;
            $last['n'] = &$newLast;

            $this->lastElement = &$newLast;
        }

        if (!$this->firstElement) {
            $this->firstElement = &$this->lastElement;
        }

        return null;
    }

    public function pop()
    {
        $value = $this->firstElement['v'];
        $this->firstElement = &$this->firstElement['n'] ?? null;

        return $value;
    }

    public function peek()
    {
        return $this->firstElement['v'];
    }

    public function empty()
    {
        return $this->firstElement === null;
    }
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * $obj = MyQueue();
 * $obj->push($x);
 * $ret_2 = $obj->pop();
 * $ret_3 = $obj->peek();
 * $ret_4 = $obj->empty();
 */
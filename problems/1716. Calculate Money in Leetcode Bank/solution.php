<?php

class Solution
{

    public function totalMoney($n): int
    {
        $total = 0;
        $start = 0;

        $dayNumber = 1;
        while ($n-- > 0) {
            $total += $dayNumber + $start;

            if ($dayNumber++ % 7 === 0) {
                $dayNumber = 1;
                ++$start;
            }
        }

        return $total;
    }

}
class Solution:
    def maxSubArray(self, nums: list[int]) -> int:
        length = len(nums)
        max = nums[0]
        if length == 1:
            return max

        prev_sum = nums[0]
        for i in range(1, length):
            value = nums[i]
            if prev_sum < 0:
                prev_sum = value
            else:
                prev_sum += value

            if prev_sum > max:
                max = prev_sum

        return max

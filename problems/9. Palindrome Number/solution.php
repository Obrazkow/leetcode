<?php

class Solution {

    /**
     * @param Integer $x
     * @return Boolean
     */
    function isPalindrome($x)
    {
        if ($x < 0) {
            return false;
        }

        $integerCopy = $x;
        $valueByRank = [];
        $maxRank = 0;
        while (true) {
            $valueByRank[++$maxRank] = $integerCopy % 10;

            $integerCopy /= 10;
            if ($integerCopy < 1) {
                break;
            }
        }

        $reversedValue = 0;
        foreach ($valueByRank as $rankNumber => $value) {
            $reversedValue += $value * pow(10, $maxRank - $rankNumber);
        }

        return $x === $reversedValue;
    }
}
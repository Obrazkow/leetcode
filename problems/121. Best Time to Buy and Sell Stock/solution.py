from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        if len(prices) == 1:
            return 0

        profit = 0
        min = prices[0]

        for price in prices:
            if price < min:
                min = price
                continue

            if price - min > profit:
                profit = price - min

        return profit
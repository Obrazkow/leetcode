<?php

class Solution
{

    public function spiralOrder($matrix)
    {
        $minY = 0;
        $maxY = count($matrix) - 1;

        $minX = 0;
        $maxX = count($matrix[$minY]) - 1;

        $result = [];
        while (true) {
            for ($x = $minX; $x <= $maxX; $x++) {
                $result[] = $matrix[$minY][$x];
            }

            if (++$minY > $maxY) {
                break;
            }

            for ($y = $minY; $y <= $maxY; $y++) {
                $result[] = $matrix[$y][$maxX];
            }

            if ($minX > --$maxX) {
                break;
            }

            for ($x = $maxX; $x >= $minX; $x--) {
                $result[] = $matrix[$maxY][$x];
            }

            if ($minY > --$maxY) {
                break;
            }

            for ($y = $maxY; $y >= $minY; $y--) {
                $result[] = $matrix[$y][$minX];
            }

            if (++$minX > $maxX) {
                break;
            }
        }

        return $result;
    }
}
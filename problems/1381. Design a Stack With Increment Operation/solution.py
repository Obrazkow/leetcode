class CustomStack:

    def __init__(self, size: int):
        self.size = size
        self.len = 0
        self.stack = []

    def push(self, x: int) -> None:
        if self.size > self.len:
            self.len += 1
            self.stack.append(x)

    def pop(self) -> int:
        if self.len == 0:
            return -1

        self.len -= 1
        return self.stack.pop()

    def increment(self, k: int, val: int) -> None:
        i = 0
        k = min(k, self.len)
        while i < k:
            self.stack[i] += val
            i += 1

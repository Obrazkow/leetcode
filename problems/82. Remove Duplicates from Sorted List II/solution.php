<?php

class Solution
{

    public function deleteDuplicates($head)
    {
        if (!$head) {
            return null;
        }

        $root = new ListNode(0);
        $end = $root;

        $last = null;
        while ($head) {
            if (!$last) {
                $last = $head;
                $head = $head->next;
                $last->next = null;
            }

            if ($head->val === $last->val) {
                while ($head) {
                    if ($head->val === $last->val) {
                        $head = $head->next;
                        continue;
                    }

                    break;
                }

                $last = null;

                continue;
            }

            $end->next = $last;
            $end = $last;
            $last = null;
        }

        if ($last) {
            $end->next = $last;
        }

        return $root->next;
    }
}
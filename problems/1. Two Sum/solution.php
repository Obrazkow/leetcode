<?php

class Solution {

    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer[]
     */
    function twoSum($nums, $target) {
        $indexByNumber = [];

        foreach($nums as $index => $number) {
            if (!isset($indexByNumber[$number])) {
                $indexByNumber[$number] = [];
            }

            $indexByNumber[$number][] = $index;
        }


        foreach($nums as $outerIndex => $number) {
            $delta = $target - $number;

            $innerIndex = $delta === $number ? 1 : 0;


            if (isset($indexByNumber[$delta][$innerIndex])) {
                return [$outerIndex, $indexByNumber[$delta][$innerIndex]];
            }
        }

        return [];
    }
}
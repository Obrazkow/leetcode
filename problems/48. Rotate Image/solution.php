<?php

class Solution
{

    public function rotate(&$matrix)
    {
        $size = count($matrix) - 1;
        $offset = 0;
        while (count($matrix) - 1 > $offset * 2) {
            for ($i = $offset; $i < $size - $offset; $i++) {
                $topRight = $matrix[$i][$size - $offset];
                $bottomRight = $matrix[$size - $offset][$size - $i];
                $bottomLeft = $matrix[$size - $i][$offset];
                $topLeft = $matrix[$offset][$i];

                $matrix[$i][$size - $offset] = $topLeft;
                $matrix[$size - $offset][$size - $i] = $topRight;
                $matrix[$size - $i][$offset] = $bottomRight;
                $matrix[$offset][$i] = $bottomLeft;
            }
            $offset++;
        }
    }

}
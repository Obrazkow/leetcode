<?php

class Solution
{

    public function uniquePaths($m, $n)
    {
        if ($m === 1 || $n === 1) {
            return 1;
        }

        $matrix = [];

        if ($n < $m) {
            $tmp = $m;
            $m = $n;
            $n = $tmp;
        }

        for ($i = 0; $i < $n; $i++) {
            $matrix[0][$i] = 1;
        }

        for ($y = 1; $y < $m; $y++) {
            for ($x = $y; $x < $n; $x++) {
                $matrix[$y][$x] = $x == $y ? $matrix[$y - 1][$x] * 2 : $matrix[$y - 1][$x] + $matrix[$y][$x - 1];
            }
        }

        return $matrix[$m - 1][$n - 1];
    }
}
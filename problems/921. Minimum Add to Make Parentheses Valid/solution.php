<?php

class Solution {

    function minAddToMakeValid($s): int
    {
        $stringLength = strlen($s);

        $leftUnclosedParentheses = 0;
        $rightUnclosedParentheses = 0;

        for ($i = 0; $i < $stringLength; $i++) {
            if ($s[$i] === '(') {
                ++$leftUnclosedParentheses;
                continue;
            }

            if ($leftUnclosedParentheses > 0) {
                --$leftUnclosedParentheses;
                continue;
            }

            ++$rightUnclosedParentheses;
        }

        return $leftUnclosedParentheses + $rightUnclosedParentheses;
    }

}
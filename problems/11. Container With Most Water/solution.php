<?php

class Solution
{


    public function maxArea($height): int
    {
        $left = 0;
        $right = count($height) - 1;

        $maxVolume = 0;
        while ($left !== $right) {
            if ($height[$left] >= $height[$right]) {
                $curVolume = ($right - $left) * $height[$right];
                $right--;
            } else {
                $curVolume = ($right - $left) * $height[$left];
                $left++;
            }

            if ($curVolume > $maxVolume) {
                $maxVolume = $curVolume;
            }
        }

        return $maxVolume;
    }
}
from typing import List


class NumMatrix:

    index: list[list[int]]

    def __init__(self, matrix: List[List[int]]):
        y_length = len(matrix)
        x_length = len(matrix[0])

        y = 0
        while y < y_length:
            x = 1
            while x < x_length:
                matrix[y][x] += matrix[y][x - 1]
                x += 1
            y += 1

        y = 1
        while y < y_length:
            x = 0
            while x < x_length:
                matrix[y][x] += matrix[y - 1][x]
                x += 1

            y += 1

        self.index = matrix

    def sumRegion(self, row1: int, col1: int, row2: int, col2: int) -> int:
        full = self.index[row2][col2]

        if row1 == 0 and col1 == 0:
            return full

        if row1 == 0 and col1 != 0:
            return full - self.index[row2][col1 - 1]

        if row1 != 0 and col1 == 0:
            return full - self.index[row1 - 1][col2]

        full_top_part = self.index[row1 - 1][col2]
        left_top_part = self.index[row1 - 1][col1 - 1]
        right_top_part = full_top_part - left_top_part
        full_left_part = self.index[row2][col1 - 1]

        return full - (full_left_part + right_top_part)

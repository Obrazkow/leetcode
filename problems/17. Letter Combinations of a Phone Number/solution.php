<?php

class Solution {


    public function letterCombinations($digits)
    {
        if ($digits === '') {
            return [];
        }

        $mapping = [
            2 => ['a', 'b', 'c'],
            3 => ['d', 'e', 'f'],
            4 => ['g', 'h', 'i'],
            5 => ['j', 'k', 'l'],
            6 => ['m', 'n', 'o'],
            7 => ['p', 'q', 'r', 's'],
            8 => ['t', 'u', 'v'],
            9 => ['w', 'x', 'y', 'z'],
        ];

        $digits = str_split($digits);

        $result = [];
        foreach($digits as $digit) {
            if (!count($result)) {
                $result = $mapping[$digit];
                continue;
            }

            $currentMapping = $mapping[$digit];
            $tmp = [];
            foreach($result as $combination) {
                foreach($currentMapping as $letter) {
                    $tmp[] = $combination . $letter;
                }
            }

            $result = $tmp;
        }

        return $result;
    }
}
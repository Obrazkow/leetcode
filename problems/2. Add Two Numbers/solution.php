<?php

/**
 * Definition for a singly-linked list.
 * class ListNode {
 *     public $val = 0;
 *     public $next = null;
 *     function __construct($val = 0, $next = null) {
 *         $this->val = $val;
 *         $this->next = $next;
 *     }
 * }
 */
class Solution {

    /**
     * @param ListNode $l1
     * @param ListNode $l2
     * @return ListNode
     */
    function addTwoNumbers($l1, $l2) {
        $node = null;
        $lastNode = null;
        $rest = 0;

        while(true) {
            if (!$l1 && !$l2 && !$rest) {
                return $node;
            }

            $sum = $rest;

            if ($l1) {
                $sum += $l1->val;
                $l1 = $l1->next;
            }

            if ($l2) {
                $sum += $l2->val;
                $l2 = $l2->next;
            }

            if ($sum > 9) {
                $rest = (int) ($sum / 10);
                $sum = $sum % 10;
            } else {
                $rest = 0;
            }

            $currentNode = new ListNode($sum, null);
            if (!$lastNode) {
                $node = $currentNode;
                $lastNode = $node;
            } else {
                $lastNode->next = $currentNode;
                $lastNode = $currentNode;
            }
        }
    }
}
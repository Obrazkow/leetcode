<?php

class Solution
{

    public function isValidSudoku($board)
    {
        $size = 9;
        $gridSize = 3;

        $xAxis = [];
        $yAxis = [];
        $grids = [];
        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size; $j++) {
                $value = $board[$i][$j];
                if ($value === '.') {
                    continue;
                }

                if (isset($xAxis[$j][$value])) {
                    return false;
                }

                if (isset($yAxis[$i][$value])) {
                    return false;
                }

                $gridNumberXAxis = (int)($j / $gridSize);
                $gridNumberYAxis = (int)($i / $gridSize);

                $gridNumber = $gridNumberXAxis + $gridNumberYAxis * $gridSize;
                if (isset($grids[$gridNumber][$value])) {
                    return false;
                }

                $xAxis[$j][$value] = 1;
                $yAxis[$i][$value] = 1;
                $grids[$gridNumber][$value] = 1;
            }
        }

        return true;
    }
}
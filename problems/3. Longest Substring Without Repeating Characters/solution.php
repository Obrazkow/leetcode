<?php

class Solution {

    /**
     * @param String $s
     * @return Integer
     */
    function lengthOfLongestSubstring($string)
    {
        $max = 0;
        $currentMax = 0;
        $notRepeatedString = '';

        for ($i = 0, $length = strlen($string); $i < $length; $i++) {
            $symbol = $string[$i];
            $pos = strpos($notRepeatedString, $symbol);
            if ($pos === false) {
                $notRepeatedString .= $symbol;
                ++$currentMax;
            } else {
                $notRepeatedString = substr($notRepeatedString, $pos + 1);
                $notRepeatedString .= $symbol;

                if ($currentMax > $max) {
                    $max = $currentMax;
                }

                $currentMax = strlen($notRepeatedString);
            }
        }

        return $currentMax > $max ? $currentMax : $max;
    }
}
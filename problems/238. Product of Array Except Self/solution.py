class Solution:
    def productExceptSelf(self, nums: list[int]) -> list[int]:
        length = len(nums)

        right_direction = {}
        left_direction = {}

        right_total = 1
        left_total = 1
        for i in range(length):
            right_total *= nums[i]
            right_direction[i] = right_total

            left_total *= nums[length - i - 1]
            left_direction[length - i - 1] = left_total

        result = []
        for i in range(length):
            total = 1
            if i > 0:
                total *= right_direction[i - 1]

            if i < length - 1:
                total *= left_direction[i + 1]

            result.append(total)

        return result


print(Solution().productExceptSelf([1, 2]))

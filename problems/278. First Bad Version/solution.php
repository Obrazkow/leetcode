<?php

/* The isBadVersion API is defined in the parent class VersionControl.
      public function isBadVersion($version){} */

class Solution extends VersionControl
{

    public function firstBadVersion($version): int
    {
        if ($version === 1) {
            return $version;
        }

        $start = 1;
        $end = $version;
        $indexOfBadVersion = $start;
        while ($end >= $start) {
            $mdl = $start + (int) (($end - $start) / 2);

            if ($this->isBadVersion($mdl)) {
                $indexOfBadVersion = $mdl;
                $end = $mdl - 1;
            } else {
                $start = $mdl + 1;
            }
        }

        return $indexOfBadVersion;
    }

}
<?php

class Solution
{

    public function sortColors(&$nums)
    {
        $length = count($nums);

        $startRed = 0;
        $startWhite = 0;
        $startBlue = $length - 1;

        for ($i = 0; $i < $length; $i++) {
            $value = $nums[$i];
            if ($value !== 0) {
                $startRed = $i;
                $startWhite = $i;
                break;
            }
        }

        for ($i = $length - 1; $i >= 0; $i--) {
            $value = $nums[$i];
            if ($value !== 2) {
                $startBlue = $i;
                break;
            }
        }

        for ($i = $startRed; $i < $length;) {
            $value = $nums[$i];
            if ($value === 0) {
                $tmp = $nums[$startRed];
                $nums[$startRed] = $value;
                $nums[$i] = $tmp;

                $startRed++;
                $startWhite++;
            } else if ($value === 2 && $i < $startBlue) {
                while ($nums[$startBlue] === 2) {
                    $startBlue--;
                }

                if ($startBlue < $i) {
                    break;
                }

                $tmp = $nums[$startBlue];
                $nums[$startBlue] = $value;
                $startBlue--;
                $nums[$i] = $tmp;
                if ($tmp === 0) {
                    continue;
                }
            }

            $i++;
        }

        return $nums;
    }
}
<?php

class Solution
{

    public function trap(array $heights): int
    {
        if (count($heights) < 2) {
            return 0;
        }

        $total = 0;
        $startPosition = 0;
        $endPosition = count($heights) - 1;

        while ($startPosition < $endPosition) {
            $startHeight = $heights[$startPosition];
            $endHeight = $heights[$endPosition];

            if ($startHeight < $endHeight) {
                for ($positionInInterval = $startPosition + 1; $positionInInterval < $endPosition; $positionInInterval++) {
                    $heightInInterval = $heights[$positionInInterval];

                    if ($heightInInterval > $startHeight) {
                        break;
                    }

                    $total += $startHeight - $heightInInterval;
                }

                $startPosition = $positionInInterval;
            } else {
                for ($positionInInterval = $endPosition - 1; $positionInInterval > $startPosition; $positionInInterval--) {
                    $heightInInterval = $heights[$positionInInterval];

                    if ($heightInInterval > $endHeight) {
                        break;
                    }

                    $total += $endHeight - $heightInInterval;
                }

                $endPosition = $positionInInterval;
            }
        }

        return $total;
    }

}
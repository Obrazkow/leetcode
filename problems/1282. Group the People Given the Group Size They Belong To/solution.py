from typing import List


class Solution:
    def groupThePeople(self, groupSizes: List[int]) -> List[List[int]]:
        groups = {}
        index = 0
        for v in groupSizes:
            if v not in groups:
                groups[v] = []

            groups[v].append(index)
            index += 1

        result = []
        for size, items in groups.items():
            itemsCount = len(items)
            if (size > itemsCount):
                continue

            if (size == itemsCount):
                result.append(items)
                continue

            i = 0
            while i < int(itemsCount / size):
                result.append(items[i * size:(i + 1) * size])
                i += 1

        return result

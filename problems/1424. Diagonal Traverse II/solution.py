from typing import List


class Solution:
    def findDiagonalOrder(self, nums: List[List[int]]) -> List[int]:
        if not len(nums):
            return []

        v_index = 0
        v_length = len(nums)

        buckets = []
        while v_index < v_length:
            h_index = 0
            h_length = len(nums[v_index])

            while h_index < h_length:
                bucket_index = v_index + h_index
                if len(buckets) <= bucket_index:
                    buckets.append([])

                buckets[bucket_index].append(nums[v_index][h_index])

                h_index += 1

            v_index += 1

        numbers = []
        for bucket in buckets:
            i = len(bucket) - 1
            while i >= 0:
                numbers.append(bucket[i])
                i -= 1

        return numbers
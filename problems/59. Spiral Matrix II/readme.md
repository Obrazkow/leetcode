[Spiral Matrix II](https://leetcode.com/problems/spiral-matrix-ii/).

Given a positive integer n, generate an n x n matrix filled with elements from 1 to n2 in spiral order.


Constraints:

- 1 <= n <= 20

![](https://assets.leetcode.com/uploads/2020/11/13/spiraln.jpg)
```
Input: n = 3
Output: [[1,2,3],[8,9,4],[7,6,5]]
```

```
Input: n = 1
Output: [[1]]
```

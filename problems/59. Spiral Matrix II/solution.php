<?php

class Solution
{

    public function generateMatrix($n)
    {
        $minY = 0;
        $maxY = $n - 1;

        $minX = 0;
        $maxX = $maxY;

        $i = 1;
        $result = [];
        while (true) {
            for ($x = $minX; $x <= $maxX; $x++) {
                $result[$minY][$x] = $i++;
            }

            if (++$minY > $maxY) {
                break;
            }

            for ($y = $minY; $y <= $maxY; $y++) {
                $result[$y][$maxX] = $i++;
            }

            if ($minX > --$maxX) {
                break;
            }

            for ($x = $maxX; $x >= $minX; $x--) {
                $result[$maxY][$x] = $i++;
            }

            if ($minY > --$maxY) {
                break;
            }

            for ($y = $maxY; $y >= $minY; $y--) {
                $result[$y][$minX] = $i++;
            }

            if (++$minX > $maxX) {
                break;
            }
        }

        foreach ($result as $key => $value) {
            ksort($result[$key]);
        }

        return $result;
    }
}
